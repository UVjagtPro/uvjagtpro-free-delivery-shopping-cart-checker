<?php

	/**
	* Plugin Name: UVjagtPro - Free delivery shopping cart checker
	* Description: This plugin checks the shopping cart amount and informs the customer, if he or she got free delivery.
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### Initial banner display #################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	function shopping_cart_checker() 
	{
	   	global $woocommerce;
	   	$free_shipping_threshold = 500;

	   	if ((WC()->cart->cart_contents_total) !== 0) : {

        	$total = WC()->cart->get_displayed_subtotal();

			if ($total < $free_shipping_threshold) : {

				$country = WC()->checkout->get_value('billing_country');
				$free_shipping_countries = array('DK'); 
				
				if (in_array($country, $free_shipping_countries)) : {

					$currentAmount = $free_shipping_threshold-$total;

					?>
			
						<div class="shopping-cart-checker">
							<p>Du mangler blot at handle for <?php echo wc_price($free_shipping_threshold-$total); ?> mere for at opnå <span class="free-delivery-label">gratis fragt!</span></p>
						</div>

					<?php
				}
				endif;

			} elseif ($total > $free_shipping_threshold) : {
				?>
			
					<div class="shopping-cart-checker">
						<p>Du har opnået <span class="free-delivery-label">gratis fragt!</span></p>
					</div>

				<?php
			}
			endif;

		} elseif ((WC()->cart->cart_contents_total) === 0) : {
			?>
			
				<div class="shopping-cart-checker">
					<p>Du kan opnå <span class="free-delivery-label">gratis fragt</span>, hvis du handler for mere end <?php echo $free_shipping_threshold; ?> <?php echo get_woocommerce_currency_symbol();?></p>
				</div>

			<?php
		}
		endif;

	} 

	add_action( 'storefront_before_content', 'shopping_cart_checker' );

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### AJAX handler ###########################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/
	
	function woocommerce_header_add_to_cart_fragment( $fragments ) 
	{
	    ob_start();

	    global  $woocommerce;
	    $free_shipping_threshold = 500;
	    
	    if ((WC()->cart->cart_contents_total) !== 0) : {

        	$total = WC()->cart->get_displayed_subtotal();

			if ($total < $free_shipping_threshold) : {

				$country = WC()->checkout->get_value('billing_country');
				$free_shipping_countries = array('DK');  
				
				if (in_array($country, $free_shipping_countries)) : {

					$currentAmount = $free_shipping_threshold-$total;

					?>
			
						<div class="shopping-cart-checker">
							<p>Du mangler blot at handle for <?php echo wc_price($free_shipping_threshold-$total); ?> mere for at opnå <span class="free-delivery-label">gratis fragt!</span></p>
						</div>

					<?php
				}
				endif;

			} elseif ($total > $free_shipping_threshold) : {
				?>
			
					<div class="shopping-cart-checker">
						<p>Du har opnået <span class="free-delivery-label">gratis fragt!</span></p>
					</div>

				<?php
			}
			endif;

		} elseif ((WC()->cart->cart_contents_total) === 0) : {
			?>
			
				<div class="shopping-cart-checker">
					<p>Du kan opnå <span class="free-delivery-label">gratis fragt</span>, hvis du handler for mere end <?php echo $free_shipping_threshold; ?> <?php echo get_woocommerce_currency_symbol();?></p>
				</div>

			<?php
		}
		endif;

	    $fragments['div.shopping-cart-checker'] = ob_get_clean();

	    return $fragments;
	}

	add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

?>
